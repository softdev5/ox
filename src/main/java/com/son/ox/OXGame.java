/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.son.ox;

import java.util.*;

/**
 *
 * @author mc-so
 */
public class OXGame {

    static Scanner kb = new Scanner(System.in);
    static String[][] table = {
        {"-", "-", "-"},
        {"-", "-", "-"},
        {"-", "-", "-"}
    };
    static String player = "X";
    static int row = 0, col = 0;
    static boolean isFinish = false;
    static String winner = "-";

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " Turn");
    }

    static void input() {
        boolean pass = false;
        while (pass == false) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (row + col >= 0 && row + col <= 6)
                pass = checkTable();
            else {
                System.out.println("Your input is out of range,"
                        + " pleas input again!");
            }
        }

    }

    static void checkRow() {
        for (int row = 0; row < table.length; row++) {
            if (!table[row][col].equals(player)) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkCol() {
        for (int col = 0; col < table[row].length; col++) {
            if (!table[row][col].equals(player)) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkBackSlash() {
        for (int col = 0; col < table[row].length; col++) {
            if (!table[col][col].equals(player)) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkSlash() {
        int count = 2;
        for (int row = 0; row < table.length; row++) {
            if (!table[row][count].equals(player)) {
                return;
            }
            count--;
        }
        isFinish = true;
        winner = player;
    }

    static void checkWinner() {
        checkRow();
        checkCol();
        checkSlash();
        checkBackSlash();
    }

    static void switchPlayer() {
        if(player=="X")
            player="O";
        else
            player="X";
    }

    static void showResult() {
        showTable();
        if(winner=="-")
            System.out.println("Tie Game!!");
        else
            System.out.println("Player "+winner+" Win!!");
    }

    static void showBye() {
        System.out.println("Bye Bye....");
    }

    static boolean checkTable() {
        if (table[row][col].equals("-")) {
            table[row][col] = player;
            return true;
        } else {
            System.out.println("This spot already used, please input again!");
            return false;
        }

    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWinner();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }

}
